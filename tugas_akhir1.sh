#!/bin/bash

while :
do
clear
#Tampil Menu A
echo "Selamat Datang - $(hostname)"
echo "======================================="
echo "++++++++++    Menu Utama     ++++++++++"
echo "======================================="
echo "A. Aritmatika"
echo "B. Tentang Pembuat"
echo "C. Keluar Program"
echo "======================================="
read -p "Pilih Opsi Menu = [ A - C ] " pilihan

# mulai case in pada menu utama
	case $pilihan in
	
	
	a | A)
		clear
		#Tampil Menu A.1
		echo "+++++++    Menu A.Aritmatika     +++++++"
		echo "========================================"
		echo "1. Penjumlahan"
		echo "2. Pengurangan"
		echo "3. Perkalian"
		echo "4. Pembagian"
		echo "5. Kembali Ke Menu Utama"
		echo "========================================"
		read -p "Pilih Opsi Menu = [ 1 - 5 ] " pilihan2
		# mulai case in pada menu Aritmatika
		case $pilihan2 in
			1)  read -p "Masukan Angka Pertama = " var1
				read -p "Masukan Angka Kedua = " var2
				hasil=$(( $var1 + $var2 ))
				echo "Hasil Dari $var1 + $var2 adalah $hasil"
				read -p "Press [Enter] key to continue..."
				readEnterKey
				bash tugas_akhir1.sh
				;;
			2) 	read -p "Masukan Angka Pertama = " var1
				read -p "Masukan Angka Kedua = " var2
				hasil=$(( $var1 - $var2 ))
				echo "Hasil Dari $var1 - $var2 adalah $hasil"
				read -p "Press [Enter] key to continue..."
				readEnterKey
				bash tugas_akhir1.sh
				;;
			3)  read -p "Masukan Angka Pertama = " var1
				read -p "Masukan Angka Kedua = " var2
				hasil=$(( $var1 * $var2 ))
				echo "Hasil Dari $var1 x $var2 adalah $hasil"
				read -p "Press [Enter] key to continue..."
				readEnterKey
				bash tugas_akhir1.sh
				;;
			4)  read -p "Masukan Angka Pertama = " var1
				read -p "Masukan Angka Kedua = " var2
				hasil=$(( $var1 / $var2 ))
				echo "Hasil Dari $var1 : $var2 adalah $hasil"
				read -p "Press [Enter] key to continue..."
				readEnterKey
				bash tugas_akhir1.sh
				;;
			5)  bash tugas_akhir1.sh
				;;
			*)
				echo "Error: Pilihan Anda Salah..."
				read -p "Tekan [Enter] Untuk Melanjutkan........"
				readEnterKey
				bash tugas_akhir1.sh
				;;
		esac
			read -p "Press [Enter] key to continue..."
			readEnterKey
			bash tugas_akhir1.sh
	;;

	b | B)
		clear
		#Tampil Menu A.2
		echo "++++++   Menu B. Tentang Pembuat   +++++"
		echo "========================================"
		echo "1. Nama, NIM, Home Dir"
		echo "2. Username@ Hostname"
		echo "3. Kembali Ke Menu Utama"
		echo "========================================"
		read -p "Pilih Opsi Menu = [ 1 - 3 ] " pilihan2
		# mulai case in pada menu Aritmatika
		case $pilihan2 in
			1)  
				echo "Nama		: Muslim Nugroho"
				echo "NIM		: 2167200825"
				echo "Home Dir		:/home/muslim/"
				read -p "Press [Enter] key to continue..."
				readEnterKey
				bash tugas_akhir1.sh
				;;
			2) 	
				echo "Hallo Selamat Datang - $(hostname)"
				read -p "Press [Enter] key to continue..."
				readEnterKey
				bash tugas_akhir1.sh
				;;
			3)  bash tugas_akhir1.sh
				;;
			*)
				echo "Error: Pilihan Anda Salah..."
				read -p "Tekan [Enter] Untuk Melanjutkan........"
				bash tugas_akhir1.sh
				readEnterKey
				;;
		esac
			read -p "Press [Enter] key to continue..."
			readEnterKey
	;;

	c | C)
		echo "Sampai Jumpa Lagi!"
		exit 0
		;;
	*)	
		echo "Error: Pilihan Anda Salah..."
		read -p "Tekan [Enter] Untuk Melanjutkan........"
		readEnterKey
		;;
	esac

done 